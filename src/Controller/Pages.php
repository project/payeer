<?php

namespace Drupal\payeer\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Pages {

	/**
   * Payeer service.
   *
   * @var \Drupal\payeer\Payeer
   */
  protected $payeer;

  /**
   * Basket service.
   *
   * @var \Drupal\basket\Basket
   */
  protected $basket;

  /**
   * Page Query Params.
   *
   * @var array
   */
  protected $query;

  /**
   * Constructs a new Payeer object.
   */
  public function __construct() {
    $this->payeer = \Drupal::service('Payeer');
    $this->basket = \Drupal::hasService('Basket') ? \Drupal::service('Basket') : NULL;
    $this->query = \Drupal::request()->query->all();
  }

	public function title(array $_title_arguments = array(), $_title = ''){
    return $this->payeer->t($_title);
  }

  public function pages($page_type) {
  	$element = [];
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
  	switch($page_type) {
  		
      case'pay':
  			$is_404 = TRUE;
  			$payment = $this->payeer->load([
          'id'      => $this->query['pay_id'] ?? '-'
       	]);
  			if(!empty($payment) && $payment->status == 'new') {
  				$is_404 = FALSE;
  				$element['form'] = \Drupal::formBuilder()->getForm('\Drupal\payeer\Form\PaymentForm', $payment);
  			}
  			if($is_404){
        	throw new NotFoundHttpException();
        }
  			break;

      case'fail':
        $element['#title'] = $this->payeer->t('Unsuccessful payment');
        // ---
        $config = \Drupal::config('payeer.settings')->get('config');
        $fail = !empty($config['fail'][$langcode]) ? $config['fail'][$langcode] : $config['fail']['en'];
        $element += [
          '#theme'       => 'payeer_pages',
          '#info'        => [
            'text'         => [
              '#type'         => 'processed_text',
              '#text'         => $fail['value'] ?? '',
              '#format'       => $fail['format'] ?? NULL,
            ],
            'type'         => 'fail'
          ],
          '#attached'    => [
            'library'      => [
               'payeer/css'
            ]
          ]
        ];
        break;

      case'success':
        $element['#title'] = $this->payeer->t('Successful payment');
        // ---
        $config = \Drupal::config('payeer.settings')->get('config');
        $success = !empty($config['success'][$langcode]) ? $config['success'][$langcode] : $config['success']['en'];
        $element += [
          '#theme'       => 'payeer_pages',
          '#info'        => [
            'text'         => [
              '#type'         => 'processed_text',
              '#text'         => $success['value'] ?? '',
              '#format'       => $success['format'] ?? NULL,
            ],
            'type'         => 'success'
          ],
          '#attached'    => [
            'library'      => [
               'payeer/css'
            ]
          ]
        ];
        break;

      case'status':
        $config = \Drupal::config('payeer.settings')->get('config');
        if(!empty($config['REMOTE_ADDR'])) {
          $REMOTE_ADDR = explode(PHP_EOL, $config['REMOTE_ADDR']);
          if (!in_array($_SERVER['REMOTE_ADDR'], $REMOTE_ADDR)) {
            throw new NotFoundHttpException();
          }
          // ---
          if (isset($_POST['m_operation_id']) && isset($_POST['m_sign'])) {
            $arHash = array(
              $_POST['m_operation_id'],
              $_POST['m_operation_ps'],
              $_POST['m_operation_date'],
              $_POST['m_operation_pay_date'],
              $_POST['m_shop'],
              $_POST['m_orderid'],
              $_POST['m_amount'],
              $_POST['m_curr'],
              $_POST['m_desc'],
              $_POST['m_status'],
              $config['m_key']
            );
            $signHash = strtoupper(hash('sha256', implode(':', $arHash)));
            if ($_POST['m_sign'] == $signHash && !empty($_POST['m_status'])) {
              list($orderId, $rand) = explode('-', $_POST['m_orderid']);
              $payment = $this->payeer->load([
                'id'      => $orderId
              ]);
              if(!empty($payment)) {
                $prePay = @unserialize($payment->data);
                $data['pre_pay'] = !empty($prePay['pre_pay']) ? $prePay['pre_pay'] : $prePay;
                // ---
                $payment->paytime = time();
                $payment->data = serialize($data);
                $payment->status = $_POST['m_status'];
                // ---
                $this->payeer->update($payment);
                // ---
                if($_POST['m_status'] == 'success') {
                  if(!empty($payment->nid) && !empty($this->basket)) {
                    if(method_exists($this->basket, 'paymentFinish')) {
                      $this->basket->paymentFinish($payment->nid);
                    }
                  }
                }
                // Alter
                \Drupal::moduleHandler()->alter('payeer_api', $payment, $_POST);
                // Trigger change payment status by order
                if(!empty($payment->nid) && !empty($this->basket)){
                  $order = $this->basket->Orders(NULL, $payment->nid)->load();
                  if(!empty($order) && $this->moduleHandler->moduleExists('basket_noty')){
                    \Drupal::service('BasketNoty')->trigger('change_payeer_status', [
                      'order'     => $order
                    ]);
                  }
                }
              }
            }
          }
          // ---
        }
        break;
  	}
  	return $element;
  }
}