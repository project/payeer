<?php

namespace Drupal\payeer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class PaymentForm extends FormBase {

  /**
   * Payeer service.
   *
   * @var \Drupal\payeer\Payeer
   */
  protected $payeer;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * Constructs a new Payeer object.
   */
  public function __construct() {
    $this->payeer = \Drupal::service('Payeer');
    $this->config = \Drupal::config('payeer.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'payeer_payment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $payment = NULL) {
    $form['#id'] = 'payment_form';
    $form['info'] = [
      '#type'         => 'inline_template',
      '#template'     => '<div class="sum"><label>{{ label }}:</label> {{ sum }} {{ currency }}</div>',
      '#context'      => [
        'label'         => $this->payeer->t('Sum'),
      ],
      '#attached'     => [
        'library'       => [
          'payeer/css'
        ]
      ]
    ];
    // ---
    $payment->result_url    = Url::fromRoute('payeer.pages', ['page_type' => 'success'], ['absolute' => TRUE])->toString();
    $payment->callback_url  = Url::fromRoute('payeer.pages', ['page_type' => 'status'], ['absolute' => TRUE])->toString();
    $payment->cancel_url    = Url::fromRoute('payeer.pages', ['page_type' => 'fail'], ['absolute' => TRUE])->toString();
    // Payment alter
    $this->basketPaymentFormAlter($form, $form_state, $payment);
    // ---
    $form['info']['#context']['currency'] = $form['#params']['currency'];
    $form['info']['#context']['sum'] = $form['#params']['amount'];
    // ---
    $form['actions'] = [
      '#type'         => 'actions',
      'submit'        => [
        '#type'         => 'submit',
        '#value'        => $this->payeer->t('Pay')
      ]
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function basketPaymentFormAlter(&$form, &$form_state, $payment) {
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $config = $this->config->get('config');
    $form['#params'] = [
      'amount'          => $payment->amount,
      'currency'        => $payment->currency,
      'm_desc'          => !empty($config['m_desc'][$langcode]) ? $config['m_desc'][$langcode] : 'Payment',
      'order_id'        => $payment->id.'-'.time().rand(0, 999),
      'server_url'      => $payment->callback_url,
      'result_url'      => $payment->result_url,
      'cancel_url'      => $payment->cancel_url,
    ];
    // Alter
    \Drupal::moduleHandler()->alter('payeer_payment_params', $form['#params'], $payment, $config);
    // ---
    $form['#params']['m_desc'] = !empty($form['#params']['m_desc']) ? base64_encode(trim($form['#params']['m_desc'])) : '';
    $form['#params']['m_sign'] = $this->mSign($form['#params'], $config);
    // ---
    $form['#action'] = 'https://payeer.com/merchant/';
    foreach ([
      'm_shop'      => $config['m_shop']              ?? '',
      'm_orderid'   => $form['#params']['order_id']   ?? '',
      'm_amoun'     => $form['#params']['amount']     ?? '',
      'm_curr'      => $form['#params']['currency']   ?? '',
      'm_desc'      => $form['#params']['m_desc']     ?? '',
      'm_sign'      => $form['#params']['m_sign']     ?? '',
      'm_params'    => $form['#params']['m_params']   ?? '',
    ] as $key => $value) {
      $form[$key] = [
        '#type'       => 'hidden',
        '#value'      => $value,
        '#parents'    => [$key]
      ];
    }
  }

  private function mSign($params, $config) {
    $arHash = [
      $config['m_shop'],
      $params['order_id']   ?? '',
      $params['amount']     ?? '',
      $params['currency']   ?? '',
      $params['m_desc']     ?? '',
      $config['m_key']
    ];
    return strtoupper(hash('sha256', implode(":", $arHash)));
  }
}
