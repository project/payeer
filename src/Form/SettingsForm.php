<?php

namespace Drupal\payeer\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;

/**
 * Class SettingsForm.
 */
class SettingsForm extends FormBase {

  /**
   * Payeer service.
   *
   * @var \Drupal\payeer\Payeer
   */
  protected $payeer;

  /**
   * Ajax info.
   *
   * @var array
   */
  protected $ajax;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * Constructs a new Payeer object.
   */
  public function __construct() {
    $this->payeer = \Drupal::service('Payeer');
    $this->ajax = [
      'wrapper'       => 'payeer_settings_form_ajax_wrap',
      'callback'      => '::ajaxSubmit'
    ];
    $this->config = \Drupal::config('payeer.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'payeer_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
   $values = $form_state->getValues();
   $form += [
      '#prefix'       => '<div id="'.$this->ajax['wrapper'].'">',
      '#suffix'       => '</div>',
      'status_messages'=> [
        '#type'         => 'status_messages'
      ]
    ];
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $languages = \Drupal::languageManager()->getLanguages();
    if(count($languages) > 1){
      $options = [];
      foreach ($languages as $language){
        $options[$language->getId()] = $language->getName();
      }
      $form['language'] = [
        '#type'         => 'select',
        '#title'        => t('Language'),
        '#options'      => $options,
        '#ajax'         => $this->ajax,
        '#default_value' => $langcode
      ];
    }
    if(!empty($values['language'])){
        $langcode = $values['language'];
    }
    $form['config'] = [
      '#tree'         => TRUE,
      'm_shop'         => [
        '#type'         => 'textfield',
        '#title'        => $this->payeer->t('ID'),
        '#default_value' => $this->config->get('config.m_shop'),
        '#required'     => TRUE
      ],
      'm_key'         => [
        '#type'         => 'textfield',
        '#title'        => $this->payeer->t('Secret key'),
        '#default_value' => $this->config->get('config.m_key'),
        '#required'     => TRUE
      ],
      'currency'      => [
        '#type'         => 'select',
        '#title'        => $this->payeer->t('Currency'),
        '#options'      => [
          'USD'           => 'USD',
          'RUB'           => 'RUB',
          'EUR'           => 'EUR',
          'BTC'           => 'BTC',
          'ETH'           => 'ETH',
          'BCH'           => 'BCH',
          'LTC'           => 'LTC',
          'DASH'          => 'DASH',
          'USDT'          => 'USDT',
          'XRP'           => 'XRP'
        ],
        '#default_value' => $this->config->get('config.currency'),
        '#required'     => TRUE
      ],
      'm_desc'        => [
        '#type'         => 'textfield',
        '#title'        => $this->payeer->t('Description').' ('.$langcode.')',
        '#default_value'=> $this->config->get('config.m_desc.'.$langcode),
        '#parents'      => ['config', 'm_desc', $langcode]
      ],
      'urls'          => [
        '#theme'        => 'table',
        '#rows'         => [
          [
            $this->payeer->t('Success URL'),
            Url::fromRoute('payeer.pages', ['page_type' => 'success'], ['absolute' => TRUE])->toString()
          ],[
            $this->payeer->t('Fail URL'),
            Url::fromRoute('payeer.pages', ['page_type' => 'fail'], ['absolute' => TRUE])->toString()
          ],[
            $this->payeer->t('Status URL'),
            Url::fromRoute('payeer.pages', ['page_type' => 'status'], ['absolute' => TRUE])->toString()
          ]
        ]
      ],
      'fail'          => [
        '#type'         => 'text_format',
        '#title'        => $this->payeer->t('Unsuccessful payment').' ('.$langcode.')',
        '#default_value'=> $this->config->get('config.fail.'.$langcode.'.value'),
        '#format'       => $this->config->get('config.fail.'.$langcode.'.format'),
        '#parents'      => ['config', 'fail', $langcode]
      ],
      'success'       => [
        '#type'         => 'text_format',
        '#title'        => $this->payeer->t('Successful payment').' ('.$langcode.')',
        '#default_value'=> $this->config->get('config.success.'.$langcode.'.value'),
        '#format'       => $this->config->get('config.success.'.$langcode.'.format'),
        '#parents'      => ['config', 'success', $langcode]
      ],
      'REMOTE_ADDR'   => [
        '#type'         => 'textarea',
        '#title'        => '$_SERVER[\'REMOTE_ADDR\']',
        '#default_value'=> $this->config->get('config.REMOTE_ADDR'),
      ]
    ];
    $form['actions'] = [
      '#type'         => 'actions',
      'submit'        => [
        '#type'         => 'submit',
        '#name'         => 'save',
        '#value'        => t('Save configuration'),
        '#attributes'   => [
            'class'         => ['button--primary']
        ],
        '#ajax'         => $this->ajax
      ]
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    if ($form_state->isSubmitted() && !$form_state->getErrors()) {
      $config = \Drupal::configFactory()->getEditable('payeer.settings');
      $configSave = $form_state->getValue('config');
      if(!empty($preConfig = \Drupal::config('payeer.settings')->get('config'))){
        if(!empty($preConfig['m_desc'])) {
          $configSave['m_desc'] += $preConfig['m_desc'];
        }
        if(!empty($preConfig['fail'])) {
          $configSave['fail'] += $preConfig['fail'];
        }
        if(!empty($preConfig['success'])) {
          $configSave['success'] += $preConfig['success'];
        }
      }
      $config->set('config', $configSave);
      $config->save();
      \Drupal::messenger()->addMessage(t('The configuration options have been saved.'));
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
